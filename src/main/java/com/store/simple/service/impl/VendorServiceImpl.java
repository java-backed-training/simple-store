package com.store.simple.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.store.simple.model.Vendor;
import com.store.simple.model.request.VendorRequest;
import com.store.simple.repository.VendorRepositoty;
import com.store.simple.service.VendorService;

@Service
public class VendorServiceImpl implements VendorService {

	@Autowired(required = true)
	private VendorRepositoty vendorRepositoty;
	
	@Override
	@Transactional
	public Vendor createVendor(VendorRequest vendorRequest) {
		Vendor vendor = new Vendor(vendorRequest);
		return vendorRepositoty.save(vendor);
	}
	
	@Override
	public List<Vendor> getAllVendor() {
		return vendorRepositoty.findAll();
	}
	
	@Override
	@Transactional
	public void addCredit(Long vendorId, double credit) {
		Optional<Vendor> vendorOptional = vendorRepositoty.findById(vendorId);
		if(vendorOptional.isEmpty()) {
			throw new RuntimeException("Vendor not found!");
		}
		Vendor vendor = vendorOptional.get();
		double ownerCredit = vendor.getCredit() + credit;
		vendor.setCredit(ownerCredit);
		vendorRepositoty.save(vendor);
	}
}
