package com.store.simple.service.impl;

import com.store.simple.model.Customer;
import com.store.simple.model.Order;
import com.store.simple.model.Product;
import com.store.simple.model.request.OrderRequest;
import com.store.simple.repository.OrderRepository;
import com.store.simple.service.CustomerService;
import com.store.simple.service.OrderService;
import com.store.simple.service.ProductService;
import com.store.simple.service.VendorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private ProductService productService;
    @Autowired
    private CustomerService customerService;
    @Autowired
    private VendorService vendorService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Order createOrder(OrderRequest request) {
        Customer customer = customerService.getCustomerById(request.getCustomerId());
        Product product = productService.getProductById(request.getProductId());
        Order order = new Order();
        order.setProduct(product);
        order.setCustomer(customer);
        order.setQuantity(request.getQuantity());
        double totalPrice = product.getPrice() * request.getQuantity();
        order.setTotalPrice(totalPrice);
        customerService.subtractCredit(request.getCustomerId(), totalPrice);
        productService.decreaseQuantity(product.getId(), request.getQuantity());
        vendorService.addCredit(product.getVendor().getId(), totalPrice);
        return orderRepository.save(order);
    }

    @Override
    public List<Order> getAllOrder() {
        return orderRepository.findAll();
    }
}
