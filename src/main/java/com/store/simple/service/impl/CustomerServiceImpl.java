package com.store.simple.service.impl;

import com.store.simple.model.Customer;
import com.store.simple.model.request.CustomerRequest;
import com.store.simple.repository.CustomerRepository;
import com.store.simple.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    CustomerRepository customerRepository;

    @Override
    public Customer createCustomer(CustomerRequest request) {
        Customer customer = new Customer(request);
        return customerRepository.save(customer);
    }

    @Override
    public List<Customer> getAllCustomer() {
        return customerRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Customer getCustomerById(Long id) {
        Optional<Customer> optionalCustomer = customerRepository.findById(id);
        if (optionalCustomer.isEmpty()) {
            throw new RuntimeException("Customer not found!");
        }
        return optionalCustomer.get();
    }

    @Override
    @Transactional
    public void subtractCredit(Long customerId, double amount) {
        Customer customer = getCustomerById(customerId);
        double newCredit = customer.getCredit() - amount;
        if (newCredit < 0) {
            throw new RuntimeException("Customer doesn't have enough money!");
        }
        customer.setCredit(newCredit);
        customerRepository.save(customer);
    }

    @Override
    @Transactional
    public Customer addCredit(Long customerId, double amount) {
        Customer customer = getCustomerById(customerId);
        double newCredit = customer.getCredit() + amount;
        customer.setCredit(newCredit);
        return customerRepository.save(customer);
    }
}
