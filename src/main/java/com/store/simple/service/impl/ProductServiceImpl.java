package com.store.simple.service.impl;

import com.store.simple.model.Product;
import com.store.simple.model.Vendor;
import com.store.simple.model.request.ProductRequest;
import com.store.simple.repository.ProductRepository;
import com.store.simple.repository.VendorRepositoty;
import com.store.simple.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired(required = true)
    private ProductRepository productRepository;

    @Autowired(required = true)
    private VendorRepositoty vendorRepositoty;

    @Override
    @Transactional
    public Product createProduct(ProductRequest productRequest) {
        Product product = new Product(productRequest);
        Optional<Vendor> vendorOptional = vendorRepositoty.findById(productRequest.getVendorId());
        if (vendorOptional.isEmpty()) {
            throw new RuntimeException("Vendor not found!");
        }
        product.setVendor(vendorOptional.get());
        return productRepository.save(product);
    }

    @Override
    @Transactional
    public Product updateProduct(Long id, ProductRequest productRequest) {
        Optional<Product> productOptional = productRepository.findById(id);
        if (productOptional.isEmpty()) {
            throw new RuntimeException("Product not found!");
        }
        Optional<Vendor> vendorOptional = vendorRepositoty.findById(productRequest.getVendorId());
        if (vendorOptional.isEmpty()) {
            throw new RuntimeException("Vendor not found!");
        }
        Product product = productOptional.get();
        product.update(productRequest);
        product.setVendor(vendorOptional.get());
        return productRepository.save(product);
    }

    @Override
    public List<Product> getAllProduct() {
        return productRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Product getProductById(Long id) {
        Optional<Product> optionalProduct = productRepository.findById(id);
        if (optionalProduct.isEmpty()) {
            throw new RuntimeException("Product not found!");
        }
        return optionalProduct.get();
    }

    @Override
    @Transactional
    public void decreaseQuantity(Long productId, int quantity) {
        Product product = getProductById(productId);
        int newQuantity = product.getCurrentQuantity() - quantity;
        if (newQuantity < 0) {
            throw new RuntimeException("Product quantity is not enough to decrease");
        }
        product.setCurrentQuantity(newQuantity);
        productRepository.save(product);
    }
}
