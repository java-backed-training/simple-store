package com.store.simple.service;

import com.store.simple.model.Customer;
import com.store.simple.model.request.CustomerRequest;

import java.util.List;

public interface CustomerService {

    Customer createCustomer(CustomerRequest request);

    List<Customer> getAllCustomer();

    Customer getCustomerById(Long id);

    void subtractCredit(Long customerId, double amount);

    Customer addCredit(Long customerId, double amount);
}
