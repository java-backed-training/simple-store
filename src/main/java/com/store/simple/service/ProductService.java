package com.store.simple.service;

import java.util.List;

import com.store.simple.model.Product;
import com.store.simple.model.request.ProductRequest;

public interface ProductService {

	Product createProduct(ProductRequest productRequest);
	
	Product updateProduct(Long id, ProductRequest productRequest);
	
	List<Product> getAllProduct();

	Product getProductById(Long id);
	
	void decreaseQuantity(Long productId, int quantity);
}
