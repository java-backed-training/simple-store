package com.store.simple.service;

import com.store.simple.model.Order;
import com.store.simple.model.request.OrderRequest;

import java.util.List;

public interface OrderService {

    Order createOrder(OrderRequest request);

    List<Order> getAllOrder();
}
