package com.store.simple.service;

import java.util.List;

import com.store.simple.model.Vendor;
import com.store.simple.model.request.VendorRequest;

public interface VendorService {

	Vendor createVendor(VendorRequest vendorRequest);
	
	List<Vendor> getAllVendor();
	
	void addCredit(Long vendorId, double credit);
}
