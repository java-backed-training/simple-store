package com.store.simple.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.store.simple.model.request.ProductRequest;

@Entity
@Table(name = "product")
public class Product {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private String name;

	private double price;

	private int currentQuantity;

	@ManyToOne
	@JoinColumn(name = "vendor_id")
	private Vendor vendor;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice(){ return price; }

	public void setPrice(double price) { this.price = price; }

	public int getCurrentQuantity() {
		return currentQuantity;
	}

	public void setCurrentQuantity(int currentQuantity) {
		this.currentQuantity = currentQuantity;
	}

	public Vendor getVendor() {
		return vendor;
	}

	public void setVendor(Vendor vendor) {
		this.vendor = vendor;
	}

	public Product() {

	}

	public Product(ProductRequest productRequest) {
		this.setName(productRequest.getName());
		this.setCurrentQuantity(productRequest.getCurrentQuantity());
		this.setPrice(productRequest.getPrice());
	}

	public Product update(ProductRequest productRequest) {
		this.setName(productRequest.getName());
		this.setCurrentQuantity(productRequest.getCurrentQuantity());
		this.setPrice(productRequest.getPrice());
		return this;
	}
}
