package com.store.simple.model.request;

public class ProductRequest {

	private String name;
	
	private int currentQuantity;

	private double price;
	
	private Long vendorId;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCurrentQuantity() {
		return currentQuantity;
	}

	public void setCurrentQuantity(int currentQuantity) {
		this.currentQuantity = currentQuantity;
	}

	public double getPrice(){ return price; }

	public void setPrice(double price){ this.price = price; }
	
	public Long getVendorId() {
		return vendorId;
	}

	public void setVendorId(Long vendorId) {
		this.vendorId = vendorId;
	}

	public ProductRequest() {
		
	}
}
