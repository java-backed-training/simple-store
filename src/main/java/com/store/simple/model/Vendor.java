package com.store.simple.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.store.simple.model.request.VendorRequest;

@Entity
@Table(name = "vendor")
public class Vendor {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	private String name;
	
	private double credit;
	
	private Boolean enabled;

	@OneToMany(mappedBy = "vendor", cascade = CascadeType.ALL)
	private List<Product> products;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getCredit() {
		return credit;
	}

	public void setCredit(double credit) {
		this.credit = credit;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public Vendor() {
		
	}
	
	public Vendor(VendorRequest vendorRequest) {
		this.setName(vendorRequest.getName());
		this.setCredit(vendorRequest.getCredit());
		this.setEnabled(vendorRequest.getEnabled());
	}
}
