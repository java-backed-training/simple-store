package com.store.simple.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.store.simple.model.Product;
import com.store.simple.model.request.ProductRequest;
import com.store.simple.service.ProductService;

@RestController
@RequestMapping("/product")
public class ProductController {

	@Autowired(required = true)
	private ProductService productService;
	
	@PostMapping("/create")
	public ResponseEntity<Product> createProduct(@RequestBody ProductRequest productRequest) {
		Product product = productService.createProduct(productRequest);
		return ResponseEntity.ok(product);
	}
	
	@PutMapping("/update")
	public ResponseEntity<Product> updateProduct(@RequestParam Long id, @RequestBody ProductRequest productRequest) {
		Product product = productService.updateProduct(id, productRequest);
		return ResponseEntity.ok(product);
	}
	
	@GetMapping("/list")
	public ResponseEntity<List<Product>> getAllProduct() {
		List<Product> products = productService.getAllProduct();
		return ResponseEntity.ok(products);
	}
}
