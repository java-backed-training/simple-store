package com.store.simple.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.store.simple.model.Vendor;
import com.store.simple.model.request.VendorRequest;
import com.store.simple.service.VendorService;

@RestController
@RequestMapping("/vendor")
public class VendorController {

	@Autowired(required = true)
	private VendorService vendorService;
	
	@PostMapping("/create")
	public ResponseEntity<Vendor> createVendor(@RequestBody VendorRequest vendorRequest) {
		Vendor vendor = vendorService.createVendor(vendorRequest);
		return ResponseEntity.ok(vendor);
	}
	
	@GetMapping("/list")
	public ResponseEntity<List<Vendor>> getAllVendor() {
		List<Vendor> vendors = vendorService.getAllVendor();
		return ResponseEntity.ok(vendors);
	}
}
