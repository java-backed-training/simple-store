package com.store.simple.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.store.simple.model.Vendor;

@Repository
public interface VendorRepositoty extends JpaRepository<Vendor, Long>{

}
